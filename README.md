# Git Secret
<img src="https://git-secret.io/images/git-secret-big.png"
     alt="pic"
     style="float: left; margin-right: 10px;" />

# Get it

git clone https://gitlab.com/oliver.mann.one/git-secret

## Things needed

`git` `git-secret` `gpg` `a gpg keypair`

## Install git-secret

### Linux
```
git clone https://github.com/sobolevn/git-secret.git git-secret 
cd git-secret 
make build PREFIX="/usr/local"
make install
```
### MacOS
```
brew install git-secret
```
### Windows
🤷(linux subsystem for windows)


## Setup

```
mkdir foo; cd foo
git init
git secret init
echo "./gitsecret/random_seed" > .gitignore
```
The gpg keypairs email must be the same as the git repository:
```
gpg --list-keys
git config user.email
```

## Secure a file

```
echo "secret" > a
echo "./a" > .gitignore
git secret tell <user@email>
git secret add a
git secret hide
```

## Read the encrypted file
```
git secret reveal
```
OR
```
git secret cat <filename>
```

## Add other users to "share" the secret files

Add their GPG Public Key to your Keyring

```
gpg --import ./public.key
git secret tell <user@email>
git secret reveal; git secret hide
```

Sources
[git-secret.io](Source)

Authors:

is171008 and is171016


